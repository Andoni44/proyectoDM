package Models;

import java.util.Calendar;

/**
 * Created by Andoni on 11/11/15.
 */
public class Task {

    public Task(String name, String description, String deadline, String subject)
    {
        this.name = name;
        this.description = description;
        this.deadline = deadline;
        this.subject = subject;
    }

    public String getTaskName()
    {
        return this.name;
    }

    public void setTaskName(String name)
    {
        this.name = name;
    }

    public String getTaskDescription()
    {
        return this.description;
    }

    public void setTaskDescription(String des)
    {
        this.description = des;
    }

    public String getTaskDeadline()
    {
        return this.deadline;
    }

    public void setTaskDeadLine(String cal)
    {
        this.deadline = cal;
    }

    public String getTaskSubject()
    {
        return this.subject;
    }

    public void setTaskSubject(String subject)
    {
        this.subject = subject;
    }

    public String toString (){
        return "Title: " + this.getTaskName() + "\nDeadline: " + this.getTaskDeadline();
    }

    private String name;
    private String description;
    private String deadline;
    private String subject;
}
