package Models;

import java.util.ArrayList;

;

import java.util.ArrayList;

/**
 * Created by Andoni on 11/11/15.
 */
public class Professor {

    public Professor(String name,String phone, String email, String tutoringHours, String office)
    {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.tutoringHours = tutoringHours;
        this.office = office;
    }

    public String getProfessorName()
    {
        return this.name;
    }

    public void setProfessorName(String name)
    {
        this.name = name;
    }

    public String getProfessorEmail()
    {
        return this.email;
    }

    public void setProfessorEmail(String email)
    {
        this.email = email;
    }

    public void setProfessorPhone(String phone)
    {

        this.phone = phone;
    }

    public String getProfessorPhone()
    {

        return this.phone;
    }

    public String getProfessorTutoringHours()
    {

        return this.tutoringHours;
    }

    public void setProfessorTutoringHours(String tu)
    {

        this.tutoringHours = tu;
    }

    public String getProfessorOffice()
    {

        return this.office;
    }

    public void setProfessorOffice(String office)
    {

        this.office = office;
    }

    public String toString(){
        return "Name: " +  this.name + "\nEmail: " + this.email;
    }

    private String name;
    private String email;
    private String phone;
    private String office;
    private String tutoringHours;

}

