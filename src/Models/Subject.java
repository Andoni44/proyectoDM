package Models;

import java.util.ArrayList;

/**
 * Created by Andoni on 11/11/15.
 */
public class Subject {

    public Subject(String name, String professor){

        this.name = name;
        this.professor = professor;
    }

    public String getSubjectName()
    {
        return this.name;
    }

    public void setSubjectName(String name)
    {
        this.name = name;
    }

    public String getProfessor()
    {

        return this.professor;
    }

    public void setProfessorList(String l)
    {

        this.professor = l;
    }

    public String toString(){return "Name: " + this.name + "\nProfessor: " + this.professor;}

    private String name;
    private String professor;

}
