package Models;

import android.app.Activity;

/**
 * Created by Jose Miguel on 1/2/2016.
 */
public class Package {

    public Professor p;
    public Subject s;
    public Task t;
    public int DBoption;
    public String oldTask;
    public String oldProfessor;
    public String oldSubject;
    public Activity actualActivity;

    public Professor getProfessor (){
        return this.p;
    }

    public Subject getSubject (){
        return this.s;
    }

    public Task getTask (){
        return this.t;
    }

    public int getDBOption(){return this.DBoption;}

    public String getOldTask () {return this.oldTask;}

    public String getOldProfessor () {return this.oldProfessor;}

    public String getOldSubject () {return this.oldSubject;}

    public void setP (Professor p){
        this.p = p;
    }

    public void setS (Subject s){
        this.s = s;
    }

    public void setT (Task t){
        this.t = t;
    }

    public void setOldTask (String tN) {this.oldTask = tN;}

    public void setOldProfessor (String pN) {this.oldProfessor = pN;}

    public void setOldSubject(String sN) {this.oldSubject = sN;}

    public void setOption (int option){
        this.DBoption = option;
    }

    public Activity getActualActivity () {
        return this.actualActivity;
    }
    public void setActualActivity(Activity act){
        this.actualActivity = act;
    }

}
