package es.uvigo.esei.dm1516.p23;

import Models.Package;
import Models.Task;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import com.example.StudentTagebuch.R;


/**
 * Created by andoni on 17/11/15.
 */
public class CreateTask extends Activity{

    private String sN = "";
    private String deadline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.createtask);
        this.sN = (String) this.getIntent().getExtras().getString("subjectName");
        final Button ok = (Button) this.findViewById(R.id.createTaskSubmit);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertTask();
                CreateTask.this.finish();
            }
        });

    }

    private void insertTask(){
      try {
            final EditText taskName = (EditText) this.findViewById(R.id.createtasknameedittext);
            final EditText description = (EditText) CreateTask.this.findViewById(R.id.createtaskdescriptionedittext);
            final DatePicker calendar = (DatePicker) CreateTask.this.findViewById(R.id.datePicker);
            String name = taskName.getText().toString();
            String desc = description.getText().toString();
            int year = calendar.getYear();
            int month = calendar.getMonth() + 1;
            int day = calendar.getDayOfMonth();
            this.deadline = day + "/" + month + "/" + year;
            Package p = new Package();
            p.setT(new Task(name, desc, this.deadline, this.sN));
            p.setOption(5);
            new InsertManager(this.getApplicationContext()).execute(p);
        }catch (NullPointerException p) {
        Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }
}
