package es.uvigo.esei.dm1516.p23;

import Models.Package;
import Models.Subject;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.example.StudentTagebuch.R;

import java.util.ArrayList;

/**
 * Created by Andoni on 13/11/15.
 */
public class CreateSubjectActivity extends Activity {

    ArrayList<String> professorsList;
    ArrayAdapter<String> spinnerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.createsubject);
            final Button ok = (Button) this.findViewById(R.id.createSubjectSubmit);
            final Spinner professorSpinner = (Spinner) this.findViewById(R.id.spinnerSubjectProfessor);
            this.professorsList = new ArrayList<String>();
            // Create an ArrayAdapter using the string array and a default spinner layout
            this.spinnerAdapter = new ArrayAdapter<String>(
                    this.getApplicationContext(),
                    android.R.layout.simple_spinner_item,
                    this.professorsList
            );
            // Specify the layout to use when the list of choices appears
            this.spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            professorSpinner.setAdapter(this.spinnerAdapter);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    insertSubject(professorSpinner);
                    CreateSubjectActivity.this.finish();
                }
            });

        } catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Package p = new Package();
        p.setOption(13);
        p.setActualActivity(CreateSubjectActivity.this);
        new InsertManager(this.getApplicationContext()).execute(p);
        professorsList.clear();
        this.spinnerAdapter.notifyDataSetChanged();
    }

    private void insertSubject(Spinner professorSpinner) {
       try {
            EditText subjectName = (EditText) this.findViewById(R.id.editTextSubjectName);
            String name = subjectName.getText().toString();
            String profName = professorSpinner.getSelectedItem().toString();
            Package p = new Package();
            p.setS(new Subject(name, profName));
            p.setOption(3);
            new InsertManager(this.getApplicationContext()).execute(p);
        } catch (NullPointerException p) {
            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }

    public void fillSpinner(String profName) {

      try {
          this.spinnerAdapter.add(profName);
      } catch (NullPointerException p) {
          Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }
}