package es.uvigo.esei.dm1516.p23;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.example.StudentTagebuch.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Jose Miguel on 1/7/2016.
 */
public class About extends Activity{
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        WebView wV = (WebView) this.findViewById(R.id.webView);
        getAuthorsPage(wV, 14);
    }
    private void getAuthorsPage(WebView wvView, int defaultFontSize)
    {
        WebSettings webSettings = wvView.getSettings();
        webSettings.setBuiltInZoomControls( true );
        webSettings.setDefaultFontSize( defaultFontSize );
        webSettings.setJavaScriptEnabled( true );
        wvView.addJavascriptInterface( new WebAppInterface( this ), "Android" );
        wvView.setWebViewClient( new WebViewClient() );
        StringBuilder builder = new StringBuilder();
        try {
            String line;
            InputStream in = this.getAssets().open( "authors.html" );
            BufferedReader inf = new BufferedReader( new InputStreamReader( in ) );
            while( ( line = inf.readLine()) != null) {
                builder.append( line );
            }
            in.close();
        } catch (IOException e) {
            builder.append( "<html><body><big>An error occurred while loading the web page</big></body></html>");
        }
        wvView.loadData( builder.toString(), "text/html", "utf-8" );
    }
}
