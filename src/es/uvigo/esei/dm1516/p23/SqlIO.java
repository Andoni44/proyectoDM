package es.uvigo.esei.dm1516.p23;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Jose Miguel on 09/12/2015.
 */
public class SqlIO extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "StudentTagebuch";
    private static final int DATABASE_VERSION = 1;

    public SqlIO(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL("CREATE TABLE IF NOT EXISTS Professors("
                    + "name varchar(30) NOT NULL PRIMARY KEY,"
                    + "phone varchar(13),"
                    + "email varchar(40),"
                    + "tutoring varchar (50),"
                    + "office varchar (5)"
                    + ")");
            db.execSQL("CREATE TABLE IF NOT EXISTS Subjects("
                    + "name varchar(50) NOT NULL PRIMARY KEY,"
                    + "ProfessorName varchar (30) NOT NULL,"
                    + "FOREIGN KEY (ProfessorName) REFERENCES Professors(name)"
                    + ")");
            db.execSQL("CREATE TABLE IF NOT EXISTS Tasks("
                    + "title varchar(50) NOT NULL PRIMARY KEY,"
                    + "description varchar(250),"
                    + "deadline varchar(50) NOT NULL,"
                    + "SubjectsName varchar(50) NOT NULL,"
                    + "FOREIGN KEY (SubjectsName) REFERENCES Subjects(name)"
                    + ")");
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w( SqlIO.class.getName(),
                "Upgrading database from version "
                        + oldVersion
                        + " to " + newVersion
                        + ", which will destroy all old data" );
        db.beginTransaction();
        try {
            db.execSQL( "DROP TABLE IF EXISTS Professors" );
            db.execSQL( "DROP TABLE IF EXISTS Subjects" );
            db.execSQL( "DROP TABLE IF EXISTS Tasks" );
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        this.onCreate( db );
    }

}


