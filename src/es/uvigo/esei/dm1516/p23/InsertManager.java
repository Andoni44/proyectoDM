package es.uvigo.esei.dm1516.p23;

import Models.Package;
import Models.Professor;
import Models.Subject;
import Models.Task;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

/**
 * Created by Jose Miguel on 1/2/2016.
 */
public class InsertManager extends AsyncTask<Package, Void, Cursor> {

    private SqlIO db;
    private Package pack = null;

    public InsertManager(Context context) {
        this.db = new SqlIO(context);
    }

    @Override
    protected Cursor doInBackground (Package... p) {

        Cursor toRet = null;
        SQLiteDatabase db = this.db.getWritableDatabase();
        Professor prof;
        Subject sub;
        Task task;
        this.pack = p[0];

        switch (p[0].getDBOption()) {
            case 1:
                prof = p[0].getProfessor();
                try {
                    db.beginTransaction();
                    db.execSQL("INSERT INTO Professors(name, phone, email, tutoring, office) VALUES( ? , ?, ? , ? , ? )",
                            new String[]{prof.getProfessorName(), prof.getProfessorPhone(), prof.getProfessorEmail(), prof.getProfessorTutoringHours(), prof.getProfessorOffice()});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case 2:
                prof = p[0].getProfessor();
                try {
                    db.beginTransaction();
                    db.execSQL("UPDATE Professors SET name = ?, phone = ?, email = ?, tutoring = ?, office = ? WHERE name = ?",
                            new String[]{prof.getProfessorName(), prof.getProfessorPhone(), prof.getProfessorEmail(), prof.getProfessorTutoringHours(), prof.getProfessorOffice(), p[0].getOldProfessor()});
                    db.execSQL("UPDATE Subjects SET ProfessorName = ? WHERE ProfessorName = ?",
                            new String[]{prof.getProfessorName(), p[0].getOldProfessor()});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case 3:
                sub = p[0].getSubject();
                try {
                    db.beginTransaction();
                    db.execSQL("INSERT INTO Subjects(name, ProfessorName) VALUES( ? , ? )",
                            new String[]{sub.getSubjectName(), sub.getProfessor()});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case 4:
                sub = p[0].getSubject();
                try {
                    db.beginTransaction();
                    db.execSQL("UPDATE Subjects SET name = ?, ProfessorName = ? WHERE name = ?",
                            new String[]{sub.getSubjectName(), sub.getProfessor(), p[0].getOldSubject()});
                    db.execSQL("UPDATE Tasks SET SubjectsName = ? WHERE SubjectsName = ?",
                            new String[]{sub.getSubjectName(), p[0].getOldSubject()});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case 5:
                task = p[0].getTask();
                try {
                    db.beginTransaction();
                    db.execSQL("INSERT INTO Tasks(title, description, deadline, SubjectsName) VALUES( ? , ?, ? , ? )",
                            new String[]{task.getTaskName(), task.getTaskDescription(), task.getTaskDeadline(), task.getTaskSubject()});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case 6:
                task = p[0].getTask();
                try {
                    db.beginTransaction();
                    db.execSQL("UPDATE Tasks Set title = ?, description = ?, deadline = ? WHERE title = ? AND SubjectsName = ?;",
                            new String[]{task.getTaskName(), task.getTaskDescription(), task.getTaskDeadline(), p[0].getOldTask(), task.getTaskSubject()});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case 7:
                prof = p[0].getProfessor();
                try {
                    db.beginTransaction();
                    db.execSQL("DELETE FROM Professors WHERE name = ?",
                            new String[]{prof.getProfessorName()});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case 8:
                sub = p[0].getSubject();
                try {
                    db.beginTransaction();
                    db.execSQL("DELETE FROM Tasks WHERE SubjectsName = ?",
                            new String[]{sub.getSubjectName()});
                    db.execSQL("DELETE FROM Subjects WHERE name = ?",
                            new String[]{sub.getSubjectName()});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case 9:
                task = p[0].getTask();
                try {
                    db.beginTransaction();
                    db.execSQL("DELETE FROM Tasks WHERE title = ? AND SubjectsName = ?",
                            new String[]{task.getTaskName(), p[0].getOldSubject()});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case 10:
                toRet = db.rawQuery("SELECT * FROM Professors", null);
                break;
            case 11:
                toRet = db.rawQuery("SELECT name, ProfessorName FROM Subjects", null);
                break;
            case 12:
                toRet = db.rawQuery("SELECT title, description, deadline, SubjectsName FROM Tasks WHERE SubjectsName = ?",
                        new String [] {p[0].getOldSubject()});
                break;
            case 13:
                toRet = db.rawQuery("SELECT name FROM Professors", null);
                break;
            case 14:
                toRet = db.rawQuery("SELECT name FROM Professors", null);
                break;
        }
        return toRet;
    }

    public void onPostExecute(Cursor curs)
    {
        switch (this.pack.getDBOption()) {
            case 10:
                ProfessorActivity actual = (ProfessorActivity) this.pack.getActualActivity();
                if (curs.moveToFirst()) {
                    do {
                        Professor professor = new Professor(curs.getString(0),
                                curs.getString(1),
                                curs.getString(2),
                                curs.getString(3),
                                curs.getString(4));
                        actual.fillListView(professor);
                    } while (curs.moveToNext());
                }
                break;
            case 11:
                MainActivity act = (MainActivity) this.pack.getActualActivity();
                if (curs.moveToFirst()) {
                    do {
                        Subject subject = new Subject(curs.getString(0), curs.getString(1));
                        act.fillListView(subject);
                    } while (curs.moveToNext());
                }
                break;
            case 12:
                tasks taskActivity = (tasks) this.pack.getActualActivity();
                if (curs.moveToFirst()) {
                    do {
                        Task task = new Task(curs.getString(0), curs.getString(1), curs.getString(2), curs.getString(3));
                        taskActivity.fillListView(task);
                    } while (curs.moveToNext());
                }
                break;
            case 13:
                CreateSubjectActivity createSubject = (CreateSubjectActivity) this.pack.getActualActivity();
                if (curs.moveToFirst()) {
                    do {
                        String profName = curs.getString(0);
                        createSubject.fillSpinner(profName);
                    } while (curs.moveToNext());
                }
                break;
            case 14:
                ModifySubject modifySubject = (ModifySubject) this.pack.getActualActivity();
                modifySubject.professorsList.clear();
                if (curs.moveToFirst()) {
                    do {
                        String profName = curs.getString(0);
                        modifySubject.spinnerAdapter.add(profName);
                    } while (curs.moveToNext());
                    modifySubject.spinnerAdapter.notifyDataSetChanged();
                }
                modifySubject.professorSpinner.setSelection(modifySubject.professorsList.indexOf(this.pack.getOldProfessor()));
                break;
        }
    }
}
