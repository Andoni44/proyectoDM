package es.uvigo.esei.dm1516.p23;

import Models.Package;
import Models.Subject;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.example.StudentTagebuch.R;

import java.util.ArrayList;

/**
 * Created by Jose Miguel on 12/28/2015.
 */
public class ModifySubject extends Activity {

    private String sN = "";
    private String sP = "";
    ArrayList<String> professorsList;
    ArrayAdapter<String> spinnerAdapter;
    Spinner professorSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createsubject);
        final Button ok = (Button) this.findViewById(R.id.createSubjectSubmit);
        this.professorSpinner = (Spinner) this.findViewById(R.id.spinnerSubjectProfessor);
        this.professorsList = new ArrayList<String>();
        this.spinnerAdapter = new ArrayAdapter<String>(
                this.getApplicationContext(),
                android.R.layout.simple_spinner_item,
                this.professorsList
        );
        this.spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        professorSpinner.setAdapter(this.spinnerAdapter);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertSubject(professorSpinner);
                ModifySubject.this.finish();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        populateTextViews();
        Package p = new Package();
        p.setOldProfessor(this.sP);
        p.setOption(14);
        p.setActualActivity(ModifySubject.this);
        new es.uvigo.esei.dm1516.p23.InsertManager(this.getApplicationContext()).execute(p);
    }
    private void populateTextViews (){
        this.sN = (String) this.getIntent().getExtras().getString("subjectName");
        this.sP = (String) this.getIntent().getExtras().getString("subjectProfessor");
        EditText subjectName = (EditText) this.findViewById(R.id.editTextSubjectName);
        subjectName.setText(this.sN);
    }

    private void insertSubject(Spinner professorSpinner) {
        try {
            EditText subjectName = (EditText) this.findViewById(R.id.editTextSubjectName);
            String name = subjectName.getText().toString();
            String profName = professorSpinner.getSelectedItem().toString();
            Package p = new Package();
            p.setS(new Subject(name, profName));
            p.setOldSubject(this.sN);
            p.setOption(4);
            new InsertManager(this.getApplicationContext()).execute(p);
        } catch (NullPointerException p) {
            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }
}
