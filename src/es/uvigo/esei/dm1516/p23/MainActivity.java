package es.uvigo.esei.dm1516.p23;

import Models.Package;
import Models.Subject;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import com.example.StudentTagebuch.R;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends Activity {

    private static final int MAX_LENGTH = 5;
    private ArrayAdapter<Subject> subjectListAdapter;
    private ArrayList<Subject> subjectList;
    private String userName = "";

    public int getSubjectList()
    {
        return this.subjectList.size();
    }

    public ArrayAdapter<Subject> getAdapter()
    {
        return this.subjectListAdapter;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            this.subjectList = new ArrayList<Subject>();
            ListView lvSubjects = (ListView) this.findViewById(R.id.subjectList);
            this.subjectListAdapter = new ArrayAdapter<Subject>(
                    this.getApplicationContext(),
                    android.R.layout.simple_selectable_list_item,
                    this.subjectList
            );

            lvSubjects.setLongClickable(true);
            lvSubjects.setClickable(true);
            lvSubjects.setAdapter(this.subjectListAdapter);
            this.registerForContextMenu(lvSubjects);
            lvSubjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Subject actual;
                    actual = MainActivity.this.subjectList.get(position);
                    Intent intent = new Intent(MainActivity.this, tasks.class);
                    intent.putExtra("subjectName", actual.getSubjectName());
                    startActivity(intent);
                }
            });
            final TextView name = (TextView) this.findViewById(R.id.userName);
            Button nameBtn = (Button) this.findViewById(R.id.userNameBtn);
            nameBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    name.setText(random());
                    MainActivity.this.userName = (String) name.getText();

                }
            });


        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }

        try {
            new Time(MainActivity.this).execute(new URL("http://api.geonames.org/timezoneJSON?lat=42.34&lng=-7.86&username=demo"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    private static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    @Override
    public void onStart(){

        super.onStart();
        Package p = new Package();
        p.setOption(11);
        p.setActualActivity(MainActivity.this);
        new InsertManager(this.getApplicationContext()).execute(p);
        subjectList.clear();
        this.subjectListAdapter.notifyDataSetChanged();
        try {
            new Time(MainActivity.this).execute(new URL("http://api.geonames.org/timezoneJSON?lat=42.34&lng=-7.86&username=demo"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Toast.makeText(getApplicationContext(),"Hour updated", Toast.LENGTH_LONG).show();

        FileInputStream file;

        try {

            file = this.openFileInput("name.cfg");//inicializamos el file al file donde habiamos guardado los datos

            BufferedReader cfg = new BufferedReader(new InputStreamReader(file));//carga all contenido en buffer
            MainActivity.this.userName = cfg.readLine();//devuleve la siguiente linea, si no hay lineas devuleve null
            TextView name = (TextView) findViewById(R.id.userName);
            name.setText(userName);
            cfg.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onStop()
    {
        super.onStop();

        FileOutputStream file;//fichero que se usará para guardar los datos

        try {

            file = this.openFileOutput("name.cfg", Context.MODE_PRIVATE);//lo ponemos como privado y le damos nombre
            PrintStream cfg = new PrintStream(file);//el fichero se puede escribir y leer
            cfg.println(MainActivity.this.userName);


            cfg.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        this.getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        try {
            switch (menuItem.getItemId()) {
                case R.id.mainMenuItemOpt1:
                    Intent intentNewSubject = new Intent(MainActivity.this, CreateSubjectActivity.class);
                    MainActivity.this.startActivity(intentNewSubject);
                    break;
                case R.id.mainMenuItemOpt2:
                    Intent intentProfessors = new Intent(MainActivity.this, ProfessorActivity.class);
                    MainActivity.this.startActivity(intentProfessors);
                    break;
                case R.id.mainMenuItemOpt3:
                    Intent intent = new Intent(MainActivity.this, About.class);
                    MainActivity.this.startActivity(intent);
                    break;
            }


        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }

        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

        try {
            super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
            if (view.getId() == R.id.subjectList) {
                AdapterView.AdapterContextMenuInfo adapterContextMenu =
                        (AdapterView.AdapterContextMenuInfo) contextMenuInfo;
                contextMenu.setHeaderTitle(MainActivity.this.subjectList.get(adapterContextMenu.position).getSubjectName());
                this.getMenuInflater().inflate(R.menu.contextmenumainactivity, contextMenu);
            }

        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        try {
            Subject actual;
            actual = this.subjectList.get(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
            final int pos = MainActivity.this.subjectList.indexOf(actual);

            switch (item.getItemId()) {
                case R.id.contextMenuMainOpt1:
                    Intent intentModifySubject = new Intent(this, ModifySubject.class);
                    intentModifySubject.putExtra("subjectName", actual.getSubjectName());
                    intentModifySubject.putExtra("subjectProfessor", actual.getProfessor());
                    startActivity(intentModifySubject);
                    break;

                case R.id.contextMenuMainOpt2:
                    MainActivity.this.subjectList.remove(pos);
                    Package p = new Package();
                    p.setS(actual);
                    p.setOption(8);
                    new InsertManager(this.getApplicationContext()).execute(p);
                    MainActivity.this.subjectListAdapter.notifyDataSetChanged();
                    break;

            }


        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }

        return  true;
    }

    public void fillListView (Subject subject){

        try {
            this.subjectListAdapter.add(subject);
        }catch (NullPointerException p) {
            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }

    public void setTimeInfo(String timeInfo) {

        final TextView time = (TextView) this.findViewById( R.id.time );
        time.setText( timeInfo );
        Toast.makeText(getApplicationContext(), "connection established", Toast.LENGTH_SHORT).show();
    }
}