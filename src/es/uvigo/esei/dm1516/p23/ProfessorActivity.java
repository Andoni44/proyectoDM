package es.uvigo.esei.dm1516.p23;
import Models.Package;
import Models.Professor;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.example.StudentTagebuch.R;

import java.util.ArrayList;


/**
 * Created by Andoni on 13/11/15.
 */
public class ProfessorActivity extends Activity {

    private ArrayAdapter<Professor> profListAdapter;
    private ArrayList<Professor> profList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.professors);
            this.profList = new ArrayList<Professor>();
            ListView lvProf = (ListView) this.findViewById(R.id.listViewProf);
            this.profListAdapter = new ArrayAdapter<Professor>(
                    this.getApplicationContext(),
                    android.R.layout.simple_selectable_list_item,
                    this.profList
            );
            lvProf.setLongClickable(true);
            lvProf.setClickable(true);
            lvProf.setAdapter(this.profListAdapter);
            this.registerForContextMenu(lvProf);
            lvProf.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Professor actual;
                    actual = ProfessorActivity.this.profList.get(position);
                    Intent intent = new Intent(ProfessorActivity.this, viewProfessor.class);
                    intent.putExtra("professorName", actual.getProfessorName());
                    intent.putExtra("professorEmail", actual.getProfessorEmail());
                    intent.putExtra("professorOffice", actual.getProfessorOffice());
                    intent.putExtra("professorPhone", actual.getProfessorPhone());
                    intent.putExtra("professorHours", actual.getProfessorTutoringHours());
                    startActivity(intent);
                }
            });
        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }

    @Override

    public void onStart(){
        super.onStart();
        Package p = new Package();
        p.setOption(10);
        p.setActualActivity(ProfessorActivity.this);
        new InsertManager(this.getApplicationContext()).execute(p);
        profList.clear();
        this.profListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try{
            super.onCreateOptionsMenu(menu);
            this.getMenuInflater().inflate(R.menu.professorsmainmenu, menu);
        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        try {
            switch (menuItem.getItemId()) {

                case R.id.professorsMainMenuOpt1:
                    Intent intent = new Intent(this, CreateProfessor.class);
                    this.startActivityForResult(intent, 1);
                    break;

            }

        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
       try {
           super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
           if (view.getId() == R.id.listViewProf) {
               AdapterView.AdapterContextMenuInfo adapterContextMenu =
                       (AdapterView.AdapterContextMenuInfo) contextMenuInfo;
               contextMenu.setHeaderTitle(ProfessorActivity.this.profList.get(adapterContextMenu.position).getProfessorName());
               this.getMenuInflater().inflate(R.menu.professorscontextmenu, contextMenu);
           }
       }catch (NullPointerException p) {

           Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
       }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        try {
            Professor actual;
            actual = this.profList.get(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
            final int pos = ProfessorActivity.this.profList.indexOf(actual);

            switch (item.getItemId()) {
                case R.id.professorContextMenuOpt1:
                    Intent intent = new Intent(this, ModifyProfessor.class);
                    intent.putExtra("professorName", actual.getProfessorName());
                    intent.putExtra("professorEmail", actual.getProfessorEmail());
                    intent.putExtra("professorOffice", actual.getProfessorOffice());
                    intent.putExtra("professorPhone", actual.getProfessorPhone());
                    intent.putExtra("professorHours", actual.getProfessorTutoringHours());
                    this.startActivity(intent);
                    break;

                case R.id.professorContextMenuOpt2:
                    ProfessorActivity.this.profList.remove(pos);
                    Package p = new Package();
                    p.setP(actual);
                    p.setOption(7);
                    new InsertManager(this.getApplicationContext()).execute(p);
                    ProfessorActivity.this.profListAdapter.notifyDataSetChanged();
                    break;
            }

        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
        return true;
    }

    public void fillListView (Professor professor){
       try {
            this.profListAdapter.add(professor);
        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }
}

