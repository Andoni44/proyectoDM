package es.uvigo.esei.dm1516.p23;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.example.StudentTagebuch.R;

/**
 * Created by Jose Miguel on 12/30/2015.
 */
public class viewTask extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewtask);
    }

    @Override
    public void onStart() {
        super.onStart();
        populateTextViews();
    }

    private void populateTextViews (){
        String sN = (String) this.getIntent().getExtras().getString("subjectName");
        String tN = (String) this.getIntent().getExtras().getString("taskName");
        String tDl = (String) this.getIntent().getExtras().getString("taskDeadline");
        String tDesc = (String) this.getIntent().getExtras().getString("taskDescription");
        TextView taskTitle = (TextView) this.findViewById(R.id.viewTaskTitle);
        TextView taskSubject = (TextView) this.findViewById(R.id.viewTaskSubject);
        TextView taskDescription = (TextView) this.findViewById(R.id.viewTaskDescription);
        TextView taskDeadline = (TextView) this.findViewById(R.id.viewTaskDeadline);
        taskTitle.setText(" " + tN);
        taskDescription.setText(" " + tDesc);
        taskDeadline.setText(" " + tDl);
        taskSubject.setText(" " + sN);
    }
}
