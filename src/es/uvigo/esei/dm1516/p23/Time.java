
package es.uvigo.esei.dm1516.p23;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Andoni on 29/12/15.
 */
public class Time extends AsyncTask<URL, Void, Boolean> {


    private MainActivity activity;
    private String time;
    public static final String LOG_TAG = "TimeFetcher";

    public Time(MainActivity activity){

        this.activity = activity;
        this.time = "time should be appear here";

    }

    protected void onPreExecute() {


    }

    @Override
    protected Boolean doInBackground(URL... urls)
    {

        InputStream is = null;
        boolean toret = false;

        ConnectivityManager connMgr = (ConnectivityManager)
                this.activity.getSystemService( Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        boolean connected = ( networkInfo != null && networkInfo.isConnected() );

        if ( !connected ) {

            this.activity.setTimeInfo( "No internet" );

        } else {

            HttpURLConnection conn =
                    null;
            try {
                conn = (HttpURLConnection) urls[ 0 ].openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            conn.setReadTimeout( 1000 /* milliseconds */ );
            conn.setConnectTimeout( 1000 /* milliseconds */ );
            try {
                conn.setRequestMethod( "GET" );
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            conn.setDoInput( true );

            try {
                conn.connect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                is = conn.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject json = null;
            try {
                json = new JSONObject( getStringFromStream( is ) );
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                if (json != null) {

                    this.time = json.getString( "time" );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            toret = true;
        }


        return toret;
    }

    private String getStringFromStream(InputStream is)
    {
        StringBuilder toret = new StringBuilder();
        String line;

        try ( BufferedReader reader = new BufferedReader( new InputStreamReader( is ) ) ) {
            while( ( line = reader.readLine() ) != null ) {
                toret.append( line );
            }
        } catch (IOException e) {
            Log.e( LOG_TAG, " in getStringFromString(): error converting net input to string"  );
        }

        return toret.toString();
    }

    @Override
    public void onPostExecute(Boolean result)
    {
        this.activity.setTimeInfo( time );
    }
}
