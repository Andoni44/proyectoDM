package es.uvigo.esei.dm1516.p23;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * Created by Jose Miguel on 1/7/2016.
 */
public class WebAppInterface {
    Context mContext;
    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
    }
    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText( mContext, toast, Toast.LENGTH_LONG ).show();
    }
}
