package es.uvigo.esei.dm1516.p23;

import Models.Package;
import Models.Professor;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.StudentTagebuch.R;

/**
 * Created by Jose Miguel on 12/29/2015.
 */
public class ModifyProfessor extends Activity {

    private String pN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.createprofessor);
        final Button ok = (Button) this.findViewById(R.id.createProfessorSubmit);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertProfessor();
                ModifyProfessor.this.finish();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        populateTextViews();
    }

    private void populateTextViews (){
        this.pN = (String) this.getIntent().getExtras().getString("professorName");
        String pE = (String) this.getIntent().getExtras().getString("professorEmail");
        String pO = (String) this.getIntent().getExtras().getString("professorOffice");
        String pP = (String) this.getIntent().getExtras().getString("professorPhone");
        String pH = (String) this.getIntent().getExtras().getString("professorHours");
        EditText profName = (EditText) this.findViewById(R.id.createProfessorNameEditText);
        EditText profPhone = (EditText) this.findViewById(R.id.createProfessorPhoneEditText);
        EditText profEmail = (EditText) this.findViewById(R.id.createProfessorEmailEditText);
        EditText profOffice = (EditText) this.findViewById(R.id.createProfessorOfficeEditText);
        EditText profHours = (EditText) this.findViewById(R.id.createProfessorTutoringHoursEditText);
        profName.setText(this.pN);
        profPhone.setText(pP);
        profEmail.setText(pE);
        profHours.setText(pH);
        profOffice.setText(pO);
    }

    private void insertProfessor (){
        try {
            final EditText profName = (EditText) this.findViewById(R.id.createProfessorNameEditText);
            final EditText profPhone = (EditText) this.findViewById(R.id.createProfessorPhoneEditText);
            final EditText profEmail = (EditText) this.findViewById(R.id.createProfessorEmailEditText);
            final EditText profOffice = (EditText) this.findViewById(R.id.createProfessorOfficeEditText);
            final EditText profHours = (EditText) this.findViewById(R.id.createProfessorTutoringHoursEditText);
            String name = profName.getText().toString();
            String phone = profPhone.getText().toString();
            String email = profEmail.getText().toString();
            String office = profOffice.getText().toString();
            String hours = profHours.getText().toString();
            Package p = new Package();
            p.setP(new Professor(name, phone, email, hours, office));
            p.setOldProfessor(this.pN);
            p.setOption(2);
            new InsertManager(this.getApplicationContext()).execute(p);

        }catch (NullPointerException p) {
            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }
}
