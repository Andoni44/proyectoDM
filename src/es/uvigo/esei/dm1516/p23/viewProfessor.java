package es.uvigo.esei.dm1516.p23;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.example.StudentTagebuch.R;

/**
 * Created by Jose Miguel on 12/30/2015.
 */
public class viewProfessor extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.viewprofessor);
    }

    @Override
    public void onStart() {
        super.onStart();
        populateTextViews();
    }

    private void populateTextViews (){
        String pN = (String) this.getIntent().getExtras().getString("professorName");
        String pE = (String) this.getIntent().getExtras().getString("professorEmail");
        String pO = (String) this.getIntent().getExtras().getString("professorOffice");
        String pP = (String) this.getIntent().getExtras().getString("professorPhone");
        String pH = (String) this.getIntent().getExtras().getString("professorHours");
        TextView profName = (TextView) this.findViewById(R.id.viewProfessorName);
        TextView profEmail = (TextView) this.findViewById(R.id.viewProfessorEmail);
        TextView profPhone = (TextView) this.findViewById(R.id.viewProfessorPhone);
        TextView profOffice = (TextView) this.findViewById(R.id.viewProfessorOffice);
        TextView profHours = (TextView) this.findViewById(R.id.viewProfessorHours);
        profName.setText(" " + pN);
        profPhone.setText(" " + pP);
        profEmail.setText(" " + pE);
        profHours.setText(" " + pH);
        profOffice.setText(" " + pO);
    }
}
