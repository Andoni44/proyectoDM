package es.uvigo.esei.dm1516.p23;

import Models.Task;
import android.app.Activity;
import Models.Package;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import com.example.StudentTagebuch.R;

/**
 * Created by Jose Miguel on 12/28/2015.
 */
public class ModifyTask extends Activity {

    private String sN = "";
    private String tN = "";
    private String deadline;
    private String description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createtask);
        final Button ok = (Button) this.findViewById(R.id.createTaskSubmit);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertTask();
                ModifyTask.this.finish();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        populateTextViews();
    }
    private void populateTextViews (){
        this.sN = (String) this.getIntent().getExtras().getString("subjectName");
        this.tN = (String) this.getIntent().getExtras().getString("taskName");
        this.deadline = (String) this.getIntent().getExtras().getString("taskDeadline");
        this.description = (String) this.getIntent().getExtras().getString("taskDescription");
        final EditText taskName = (EditText) this.findViewById(R.id.createtasknameedittext);
        final EditText description = (EditText) ModifyTask.this.findViewById(R.id.createtaskdescriptionedittext);
        final DatePicker calendar = (DatePicker) ModifyTask.this.findViewById(R.id.datePicker);
        taskName.setText(this.tN);
        description.setText(this.description);
        String [] dl = this.deadline.split("/");
        calendar.updateDate(Integer.parseInt(dl[2]),Integer.parseInt(dl[1]) - 1, Integer.parseInt(dl[0]));
    }

    private void insertTask(){
        try {
            final EditText taskName = (EditText) this.findViewById(R.id.createtasknameedittext);
            final EditText description = (EditText) ModifyTask.this.findViewById(R.id.createtaskdescriptionedittext);
            final DatePicker calendar = (DatePicker) ModifyTask.this.findViewById(R.id.datePicker);
            String name = taskName.getText().toString();
            String desc = description.getText().toString();
            int year = calendar.getYear();
            int month = calendar.getMonth() + 1;
            int day = calendar.getDayOfMonth();
            this.deadline = day + "/" + month + "/" + year;
            Package p = new Package();
            p.setT(new Task(name, desc, this.deadline, this.sN));
            p.setOldTask(this.tN);
            p.setOption(6);
            new InsertManager(this.getApplicationContext()).execute(p);
        }catch (NullPointerException p) {
            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }
}
