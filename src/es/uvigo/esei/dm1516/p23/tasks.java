package es.uvigo.esei.dm1516.p23;

import Models.Package;
import Models.Task;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.example.StudentTagebuch.R;

import java.util.ArrayList;

/**
 * Created by Andoni on 13/11/15.
 */
public class tasks extends Activity {

    private ArrayAdapter<Task> tasksListAdapter;
    private ArrayList<Task> tasksList;
    private String sN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.tasks);
            this.tasksList = new ArrayList<Task>();
            ListView lvTasks = (ListView) this.findViewById(R.id.listViewTasks);
            this.tasksListAdapter = new ArrayAdapter<Task>(
                    this.getApplicationContext(),
                    android.R.layout.simple_selectable_list_item,
                    this.tasksList
            );
            lvTasks.setLongClickable(true);
            lvTasks.setAdapter(this.tasksListAdapter);
            this.registerForContextMenu(lvTasks);
            lvTasks.setClickable(true);
            this.sN = (String) this.getIntent().getExtras().getString("subjectName");
            TextView tv = (TextView) this.findViewById(R.id.taskSubjectNameHeader);
            tv.setText(this.sN);
            lvTasks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Task actual;
                    actual = tasks.this.tasksList.get(position);
                    Intent intent = new Intent(tasks.this, viewTask.class);
                    intent.putExtra("subjectName", actual.getTaskSubject());
                    intent.putExtra("taskDescription", actual.getTaskDescription());
                    intent.putExtra("taskDeadline", actual.getTaskDeadline());
                    intent.putExtra("taskName", actual.getTaskName());
                    startActivity(intent);
                }
            });
        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        Package p = new Package();
        p.setOption(12);
        p.setActualActivity(tasks.this);
        p.setOldSubject(this.sN);
        new InsertManager(this.getApplicationContext()).execute(p);
        this.tasksList.clear();
        this.tasksListAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        this.getMenuInflater().inflate(R.menu.taskmenu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch(menuItem.getItemId()){

            case R.id.taskMenuOpt1:

                Intent intentNewTask = new Intent(this, CreateTask.class);
                intentNewTask.putExtra("subjectName", this.sN);
                startActivity(intentNewTask);


                break;
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        try {
            super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
            if (view.getId() == R.id.listViewTasks) {
                AdapterView.AdapterContextMenuInfo adapterContextMenu =
                        (AdapterView.AdapterContextMenuInfo) contextMenuInfo;
                contextMenu.setHeaderTitle(tasks.this.tasksList.get(adapterContextMenu.position).getTaskName());
                this.getMenuInflater().inflate(R.menu.taskcontextmenu, contextMenu);
            }
        }catch (NullPointerException p) {

            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        Task actual;
        actual = this.tasksList.get(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
        final int pos = tasks.this.tasksList.indexOf(actual);

        switch(item.getItemId()){
            case R.id.taskContextMenuOpt1:
                Intent intentModifyTask = new Intent(this, ModifyTask.class);
                intentModifyTask.putExtra("taskName", actual.getTaskName());
                intentModifyTask.putExtra("taskDescription", actual.getTaskDescription());
                intentModifyTask.putExtra("taskDeadline", actual.getTaskDeadline());
                intentModifyTask.putExtra("subjectName", this.sN);
                startActivity(intentModifyTask);
                break;

            case R.id.taskContextMenuOpt2:
                tasks.this.tasksList.remove(pos);
                Package p = new Package();
                p.setT(actual);
                p.setOldSubject(this.sN);
                p.setOption(9);
                new InsertManager(this.getApplicationContext()).execute(p);
                tasks.this.tasksListAdapter.notifyDataSetChanged();
                break;
        }
        return true;
    }

    public void fillListView (Task task){
        try {
            this.tasksListAdapter.add(task);
        }catch (NullPointerException p) {
            Toast.makeText(getApplicationContext(), "Please, initialize before creating objects", Toast.LENGTH_LONG).show();
        }
    }


}
