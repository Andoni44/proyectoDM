package es.uvigo.esei.dm1516.p23;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;


/**
 * Created by andoni on 28/12/15.
 */
public class TaskCheckerService extends Service {

    private String dl = "";
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        Toast.makeText(getApplicationContext(), "Service has been created", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {

        Toast.makeText(getApplicationContext(), "Service has been destroyed", Toast.LENGTH_LONG).show();
    }
}
